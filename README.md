#include <stdlib.h>
#include <stdio.h>
#include <curl/curl.h>

//Definimos algunos valores que usaremos en el programa
#define TRUE 1
#define FALSE 0

//definimos la estructura que usaremos en la pila
typedef struct _nodo {
	char valor;
	struct _nodo *siguiente;
} tipoNodo;

typedef tipoNodo *pNodo;
typedef tipoNodo *Pila;

/* Funciones con pilas: */
void Push(Pila *l, char v);
char Pop(Pila *l);
void Agentes_Muertos(char Agente);
void MostrarAgentesMuertos(char Agente);

//definimos la estructura para la lista doblemente enlazada
typedef struct nodo{
    int dato;
    struct nodo *siguiente;
    struct nodo *anterior;
} NODO;

//funciones con lista doblemente enlazada
NODO *CrearNodo(int dato);
int InsertarInicio(NODO **cabeza, int dato);
int InsertarFinal(NODO **cabeza, int dato);
int BuscarEnLista(NODO **cabeza, int dato);
void ImprimirLista(NODO *cabeza);

//funciones con las que el juego funcionara
int CreaMatriz(int **matriz);
void ImprimeMatriz(int **matriz);
void LimpiarMatriz(int **matriz);
int Agentes(int **matriz,int segundos);
int MoverMatriz(int **matriz);
int Balas(int **matriz,int opcion);
void SolucionLaberinto(int **matriz);
void MostrarSolucion();
int MoverBalas(int **matriz,int opcion);
//void SumaBalas(int **matriz);
int ramdomBalas(int **matriz);
int MurioAgente(int **matriz);
int tomabalas(int **matriz);
int moverAgentes(int **matriz);
int yadebemorir(int **matriz);
int disparaAgente(int **matriz);
int hayBalas(int **matriz);//si hay balas de algun agente
int MovBalaAutomatico(int **matriz);
int EncontrarAgente(int **matriz,int Agente);
int hayBalasdejugador(int **matriz);
int destruirbala(int **matriz);
int destruirbotin(int **matriz);


void DescargarPDF();
int cuantas_palabras_hay();
void Compara();

//variables globales
int opcion;
int Vida=100;

/*main desde donde se correra nustro programa*/
int main(){
	//descarga los pdf de la asamblea legislativa
		DescargarPDF();
		cuantas_palabras_hay();
		Compara();
	  /*variables que usaremos en nuestro main*/
    int CantidadBalas=40;//variable balas
    int segundos=0,opcion1,indice=0;
    int i, nfilas = 10, ncols = 10;
    int numero;

		//inicializamos matriz  dinamica
    int **matriz;
    matriz = (int **) malloc(nfilas * sizeof (int*));//reservamos memoria para cada fila
    for (i = 0; i < nfilas; i++) {
        matriz[i] = (int*) malloc(ncols * sizeof (int));//reservamos memoria para cada columna
    }

   /*int num=11;
   // Modifico el tamaño de las filas a 11
   matriz = (int **) realloc (matriz,sizeof(int)*(num));

   //Modifico el tamaño de las columnas a 11
   for (indice = 0; indice < (num); indice++){
      //matriz [indice] = (int *)realloc(*matriz,(num));
      matriz [indice] = (int*)malloc (num * sizeof(int));
   }*/


    //imprimimos la matriz principal con las estadisticas balas y vida incializadas
    CreaMatriz(matriz);//crea matriz inicial
    ImprimeMatriz(matriz);//imprime matriz

	  printf("Cantidad de balas inicial: %i\n",CantidadBalas);//muestra cantidad de balas inicial
    printf("Vida Total: %i\n",Vida);//vida inicial
    printf("\n");

    while (matriz[1][1] != 2 && Vida>0) {//mientras no haya encontrado la salida de laberinto y tenga vida
			 /*aca mostramos al usuario un menu de opciones las principales que podra realizar en el juego*/
        printf("1.Mover mi caracter\n2.Disparar\n3.Ayuda\n4.Salir\n");
        printf("Digite la opcion que desea realizar: ");
        scanf("%d", &opcion1);
        system("clear");//limpia la pantalla

				/*si bala del agente muere sin antes el jugador haberla destruido es decir disparado a la bala enemiga
				entonces perdera vida por que agente callo ensima de la mina y esta exploto*/
        int balaAgente = hayBalas(matriz);
        if (balaAgente == FALSE && opcion1!=2) {//sirve pero hay que solucionar ya que las balas salen de por medio
            Vida-=5;                          //entonces resta cuando no ahi balas
        }

        if(opcion1==1){
          /*funciones que nos ayudaran a saber si un agente esta vivo o no
					 si el jugador a tomado el botin de balas y
					 si en el terreno de juego hay balas de agentes*/
          int agentevida =MurioAgente(matriz);
          int balas = tomabalas(matriz);
          int balaAgente = hayBalas(matriz);

            /*si los segudos terminan en 5 o 0 quiere decir que es un multiplo de 5 asi que tomaremos esto como
            una guia para cada 5 segundos sacar un agente ademas en caso de que el agente muera antes de terminar
            sus 5 segundos debera salir un nuevo agente*/
            if (segundos%10==5 || segundos%10==0 || agentevida==FALSE) {
                destruirbala(matriz);//si el agente murio o ya acabo su tiempo su bala tambien debe ser destruida
                yadebemorir(matriz);//cada 5 segundos si el agente no muere este desaparece
                Agentes(matriz,segundos);//pondra el nuevo agente serca de jugador
            }
            /*printf("Agentes muertos: ");
            Agentes_Muertos('x');*/
            moverAgentes(matriz);

            /*pedimos al jugador que ingrese la direccion en la que se desea mover*/
            printf("Mover: ");
            char C = getchar();
						//funcion que se encarga de realizar el movimiento
            MoverMatriz(matriz);

            /*Si ningun agente a disparado y hay un agente entonces este debe disparar*/
            if (balaAgente == FALSE && agentevida==TRUE){
                 disparaAgente(matriz);
            }

            /*cada 5 "segundos" saldra un botin de balas y el que estaba se eliminara
						si se llega a los 5 segundos y el botin de bala uan esta == TRUE entonces lo eliminamos y
						sacamos uno nuevo*/
            if (segundos%10!=5 && segundos%10!=0 && balas==FALSE) {
                if (CantidadBalas>40) {//si cantidad de balas es mayor a 40
                    CantidadBalas=40;//las balas volveran a ser 40
                }else{
                  CantidadBalas+=5;//si no se le suma 5 balas mas
                }


            }else{
              destruirbotin(matriz);//destruye el botin de balas
              ramdomBalas(matriz);//aleatorio balas
            }

            /*mueve las balas del jugador despues de ser disparadas y seguidamente imprimimos el laberinto*/
            MoverBalas(matriz,opcion);//mueve las balas del jugador
            ImprimeMatriz(matriz); //imprime la matriz
            segundos++;//llevamos los "segundos" para asi saber cuando debemos hacer aparecer un nuevo agente

            //estadisticas del jugador vida y balas
            printf("Balas: %i\n",CantidadBalas);
            printf("Vida Total: %i\n",Vida);
            printf("\n");

        }

				//aca se ejecutan los disparos
        else if (opcion1==2){
						//cada ves que el jugador dispare balas disminuira en 1
            CantidadBalas--;

						//menu de opciones que le indicara al jugador hacia donde desea realizar el disparo
            printf("\n1.Disparar hacia la derecha\n2.Dispara hacia la izquierda\n3.Disparar hacia abajo\n4.Disparar hacia arriba\n");
            printf("Digite la opcion que desea realizar: ");
            scanf("%d", &opcion);
            system("clear");//limpiamos pantalla

						//dispramos a la derecha
            if (opcion==1){
                printf("Disparar: ");
                char balas = getchar();

                Balas(matriz,opcion);//modifica la matriz para que apresca la bala
                ImprimeMatriz(matriz); //imprime la matriz
								//estadistica vida balas
                printf("Balas: %i\n",CantidadBalas);
                printf("Vida Total: %i\n",Vida);
                printf("\n");
            }

						//disparamos a la izquierda
            else if (opcion ==2){
                printf("Disparar: ");
                char balas = getchar();

                Balas(matriz,opcion);
                ImprimeMatriz(matriz); //imprime la matriz
								//estadisticas
                printf("Balas: %i\n",CantidadBalas);
                printf("Vida Total: %i\n",Vida);
                printf("\n");
            }

						//dispramos abajo
            else if (opcion ==3){
                printf("Disparar: ");
                char balas = getchar();

                Balas(matriz,opcion);
                ImprimeMatriz(matriz); //imprime la matriz
								//estadisticas
                printf("Balas: %i\n",CantidadBalas);
                printf("Vida Total: %i\n",Vida);
                printf("\n");
            }

						//disparos arriba
            else if (opcion ==4){
                printf("Disparar: ");
                char balas = getchar();

                Balas(matriz,opcion);
                ImprimeMatriz(matriz); //imprime la matriz
								//estadisticas
                printf("Balas: %i\n",CantidadBalas);
                printf("Vida Total: %i\n",Vida);
                printf("\n");
            }
            else{

            }
        }

        else if (opcion1==3){
            MostrarSolucion();//muestra la solucion del laberinto

            //hacer pausa de un segundo
            system("/usr/bin/clear");//limpia la pantalla
        }
        else if (opcion1==4) {
           //return exit;//salimos del programacion
        }
        else{
             printf("la opcion digitada no es correcta\n");
        }
    }//termina el while
    LimpiarMatriz(matriz);//limpia la memoria
		printf("\nFelicidades encontraste la Salida\n");
		printf("\n¿Desea seguir jugando?, se debera descargar una semana mas de actas de la ALCR(Asamblea legislativa de Costa Rica)\n");
		printf("\nDigite 's' para continuar y 'n' para salir\n");

		char* final[10];
		//fflush(stdin);
		scanf("%c", &final);		//le pide al usuario informacion

		if (strcmp(final,"s")==0){		//vuelve a juegar
				return main();
		} else if (strcmp(final,"n")==0){
				printf("\n");
		}else{
				return 0;
		}


    //return 0;
}

/*Esta funcion crea la matriz laberinto la rellena con unos y a x posiciones les coloca
ceros los cuales representara los caminos del laberinto, colocara un 2 tambien el cual sera el que
representara al jugador*/
int CreaMatriz(int **matriz){
	//filas y columnas ==10
    int i,j, nfilas = 10, ncols= 10;
    // llenamos matriz
    for (i = 0; i < nfilas; i++) {
        for (j = 0; j < ncols; j++) {
            matriz[i][j] = 1;//rellana la matriz con unos

            matriz[1][0] = 2;
            matriz[1][1] = 0;
            matriz[1][2] = 0;
            matriz[1][3] = 0;
            matriz[2][3] = 0;
            matriz[2][4] = 0;
            matriz[2][5] = 0;
            matriz[2][7] = 0;
            matriz[1][6] = 0;

            matriz[1][7] = 0;
            matriz[3][5] = 0;
            matriz[4][5] = 0;
            matriz[4][2] = 0;
            matriz[3][3] = 0;
            matriz[4][3] = 0;
            matriz[4][1] = 0;
            matriz[5][1] = 0;
            matriz[5][3] = 0;
            matriz[6][3] = 0;
            matriz[6][2] = 0;

            matriz[7][2] = 0;
            matriz[8][2] = 0;
            matriz[8][3] = 0;
            matriz[8][4] = 0;
            matriz[8][5] = 0;
            matriz[8][6] = 0;
            matriz[8][7] = 0;
            matriz[7][7] = 0;

            matriz[6][7] = 0;
            matriz[5][7] = 0;
            matriz[4][7] = 0;
            matriz[3][7] = 0;
            matriz[3][8] = 0;
            matriz[3][9] = 0;
        }
    }
    return **matriz;
}


/*Esta funcion nos ayudara a desplazarnos por el laberinto usaremos getchar el cual lee una tecla
en codigo ascii y dependiendo de la tecla presionada el se movera en esa direccion lo tedioso de haber usado
getchar es que cada ves que presionemos una tecla deberemos presionar enter seguidamente para que la accion
se ejecute correctamente. Recorremos la matriz si encontramos un 2 que es el numero que representa al jugador
entonces preguntamos hacia donde desea moverse si es hacia arriba restamos uno a filas si es hacia bajo le sumamos uno a filas
si es hacia la izquierda restamos uno a columnas y si es hacia la derecha sumamos uno a las columnas*/
int MoverMatriz(int **matriz){
    int i = 0,j=0;
    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            if (matriz[i][j] == 2) {
                char C = getchar();
                switch(C){
                    case 54://derecha
                        matriz[i][j] = 0;
                        if (matriz[i][j+1]!=1){
                        matriz[i][j+1] = 2;
                        }
                        else{
                          matriz[i][j] = 2;
                        }

                        break;
                    case 50://abajo
                        matriz[i][j] = 0;
                        if (matriz[i+1][j]!=1){
                             matriz[i+1][j] = 2;
                        }else{
                          matriz[i][j] = 2;
                        }

                        break;
                    case 56://arriba
                        matriz[i][j] = 0;
                        if (matriz[i-1][j]!=1){
                            matriz[i-1][j] = 2;
                        }else{
                          matriz[i][j] = 2;
                        }

                        break;
                    case 52://izquierda
                        matriz[i][j] = 0;
                        if (matriz[i][j-1]!=1){
                            matriz[i][j-1] = 2;
                        }else{
                          matriz[i][j] = 2;
                        }

                        break;
                }
            }else{

            }
        }
    }
    return **matriz;
}


/* Aparecera un agente dependiendo de la posicion del jugador se empezara validando si el agente puede aprecer al frente
del jugador si este cumple las condiciones para aprecer al frente Aparecera si no validaremos si el segundo agente
puede aparecer que seria arriba del jugador si cumple los requisitos este aparecera si el agente no los cumplio valiamos que
el tercero los cumpla el cual seria aparecer detras del jugador si no cumple validamos si el cuarto puede aparecer que seria abajo
del jugador si este no cumpliera los requisitos saldria el quinto que aparece arriba del jugador pero 3 posiciones
mas arriba que el jugador */
int Agentes(int **matriz,int segundos){
    int i,j;

    for(i=0; i<10; i++){
        for (j = 0; j < 10; j++){
          if (matriz[i][j]==2 ){
              if (j+3<10 && matriz[i][j+3]==0 && matriz[i][j+3]!=2){
                   matriz[i][j+3]=3;
              }
              else if (i+3 < 10 && matriz[i+3][j]==0 && matriz[i+3][j]!=2) {
                matriz[i+3][j]=6;
              }
              else if (j-1>0 && matriz[i][j-1]==0  && matriz[i][j-1]!=2) {
                matriz[i][j-1]=7;
              }
              else if ((i-1) > 0 && matriz[i-1][j]==0 && matriz[i-1][j]!=2) {
                matriz[i-1][j]=8;
              }
              else if ((i-3) <10 && matriz[i-3][j]==0 && matriz[i-3][j]!=2) {
                matriz[i-3][j]=10;
              }
              else{

              }
          }

        }
    }
    return **matriz;
}

/*En esta funcion los agentes se moveran en distintas direcciones, cada agente tendra un movimiento
el movimiento que realize el agente dependera de si a donde se desea mover es una posicion valida aasi que el agente
revisara hacia donde se puede mover revisando sus 4 posibles movimientos arriba abajo izquierda derecha siempre se movera a
la primera opcion valida es decir si valido si podia mover arriba y si cumplia los requisitos est se movera */
int moverAgentes(int **matriz){
  int i,j;

  for(i=0; i<10; i++){
      for (j = 0; j < 10; j++){
        if (matriz[i][j]==7 ){
            if (matriz[i][j-1]==0){
                 matriz[i][j]=0;
                 matriz[i][j-1]=7;
            }
            else if (matriz[i-1][j]==0) {
                matriz[i][j]=0;
                matriz[i-1][j]=7;

            }else if (matriz[i][j+1]==0) {
								matriz[i][j]=0;
								matriz[i][j+1]=7;

						}else if (matriz[i+1][j]==0){
	                 matriz[i][j]=0;
	                 matriz[i+1][j]=7;
	            }




        }else if (matriz[i][j]==8 ){
            if (matriz[i+1][j]==0){
                 matriz[i][j]=0;
                 matriz[i+1][j]=8;
            }
            else if (matriz[i][j+1]==0) {
                matriz[i][j]=0;
                matriz[i][j+1]=8;

            }else if (matriz[i][j-1]==0){
                 matriz[i][j]=0;
                 matriz[i][j-1]=8;

            }else if (matriz[i-1][j]==0) {
	                matriz[i][j]=0;
	                matriz[i-1][j]=8;

	            }




        }else if (matriz[i][j]==3 ){
            if (matriz[i+1][j]==0){
                 matriz[i][j]=0;
                 matriz[i+1][j]=3;
                 return ;
            }
            else if (matriz[i][j-1]==0) {
                matriz[i][j]=0;
                matriz[i][j-1]=3;

            }else if (matriz[i-1][j]==0) {
                matriz[i][j]=0;
                matriz[i-1][j]=3;

            }else if (matriz[i][j+1]==0) {
	                matriz[i][j]=0;
	                matriz[i][j+1]=3;

	            }



        }else if (matriz[i][j]==6 ){
            if (matriz[i+1][j]==0){
                 matriz[i][j]=0;
                 matriz[i+1][j]=6;
                 //return ;//este return es para que se salga del ciclo
            }
            else if (matriz[i][j-1]==0) {
                matriz[i][j]=0;
                matriz[i][j-1]=6;
            }
            else if (matriz[i-1][j]==0) {
                matriz[i][j]=0;
                matriz[i-1][j]=6;

            }else if (matriz[i][j+1]==0) {
	                matriz[i][j]=0;
	                matriz[i][j+1]=6;

	            }




        }else if (matriz[i][j]==10 ){
              if (matriz[i-1][j]==0){
                  matriz[i][j]=0;
                  matriz[i-1][j]=10;
                  //return ;//este return es para que se salga del ciclo
              }
              else if (matriz[i][j-1]==0) {
                  matriz[i][j]=0;
                  matriz[i][j-1]=10;

              }else if (matriz[i][j+1]==0) {
		                matriz[i][j]=0;
		                matriz[i][j+1]=10;

		          }else if (matriz[i+1][j]==0){
	                 matriz[i][j]=0;
	                 matriz[i+1][j]=3;
	                 return ;
	            }
        }
      }
  }
  return **matriz;
}

/*esta funcion me servira para  cuando debo sacar un agente del terreno
de juego ya que pasado 5 segundos el agente debe salir del terreno
esta funcion la uso en el main opcion 1 mover mi caracter*/
int yadebemorir(int **matriz){
  int i,j;
  for (i=0;i<10;i++){
      for (j = 0; j < 10; j++) {
          if(matriz[i][j]==3 || matriz[i][j]==6 || matriz[i][j]==7 || matriz[i][j]==8){
              matriz[i][j]=0;
          }
      }
  }
  return **matriz;
}


/*esta funcion es la que permitira disparar en las 4 distintas direcciones arriba abajo derecha izquierda si el jugador
presiona la opcion disparar se le mostrara en el main un menu de opcion con los 4 disparos posibles dependiendo del disparo
que el jugador realice la bala aparecera en alguno de los 4 posibles lados del jugador si quiere disparar a la derecha es
la opcion 1 entonces la bala saldra a su derecha si es opcion 4 la bala saldra a la izquierda opcion 8 la bala sale arriba
opcion 2 la bala saldra bajo del jugador*/
int Balas(int **matriz,int opcion){
    int i = 0,j=0;
    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            if (matriz[i][j]==2){//dependiendo simpre del jugar ahi saldra la bala
                char balas=getchar();
                if (balas==53 && opcion==1 && matriz[i][j+1]!=1){
                    matriz[i][j+1]=4;
                }
                else if (balas==53 && opcion==2 && matriz[i][j-1]!=1){
                    matriz[i][j-1]=4;
                }
                else if (balas==53 && opcion==3 && matriz[i+1][j]!=1){
                    matriz[i+1][j]=4;
                }
                else if (balas==53 && opcion==4 && matriz[i-1][j]!=1){
                    matriz[i-1][j]=4;
                }

            }
       }
    }
    return **matriz;
}


/*esta funcion nos returnara verdadero sin hay alguna bala de jugador el terreno de juego y
falso si no la hay*/
int hayBalasdejugador(int **matriz){//si hay balas del jugador
    int i,j;
    for (i=0;i<10;i++){
        for (j = 0; j < 10; j++) {
            if(matriz[i][j]==4){//si hay una bala
                return TRUE;
            }
        }
    }
    return FALSE;
}


/*Esta funcion mueve las balas de jugador hacias atras adelante arriba o abajo las balas que se mueven hacia la derecha y
las que se mueven  hacia bajo se guia con el caracter del jugador para saber que deben  de moverse un espacio al frente del
jugador y las que se mueven hacias la izquierda y las que van hacia arriba se mueven con respecto a donde esta la bala en
ese momento si hay una bala del jugador en el terreno de juego esta se podra mover*/
int MoverBalas(int **matriz,int opcion){

	int tiros=hayBalasdejugador(matriz);
    int i,j;	//inicializo fila, columna

    for(i=0; i<10; i++){		//recorre la matriz
        for (j = 0; j < 10; j++){

			if (tiros==TRUE){

				if (matriz[i][j]==4 && opcion==1){

					if(matriz[i][j+1]==9 || matriz[i][j+1]==3
					|| matriz[i][j+1]==6 || matriz[i][j+1]==7 || matriz[i][j+1]==8
					|| matriz[i][j+1]==10){

						matriz[i][j+1]=0;	//destruye la mina
						matriz[i][j]=0;
						return 0;
					}
					else if (matriz[i][j+1]==0){
						//matriz[i][j+1]=4;	//desaparece la bala
						matriz[i][j]=0;
					}
				}
				else if(matriz[i][j]==4 && opcion==2){

					if(matriz[i][j-1]==9 || matriz[i][j-1]==3
					|| matriz[i][j-1]==6 || matriz[i][j-1]==7 || matriz[i][j-1]==8
					|| matriz[i][j-1]==10){

						matriz[i][j-1]=0;	//destruye la mina
						matriz[i][j]=0;
						return 0;
					}
					else if (matriz[i][j-1]==0){
						//matriz[i][j-1]=4;	//desaparece la bala
						matriz[i][j]=0;
						return 0;
					}
				}
				else if(matriz[i][j]==4 && opcion==3){

					if(matriz[i+1][j]==9 || matriz[i+1][j]==3
					|| matriz[i+1][j]==6 || matriz[i+1][j]==7 || matriz[i+1][j]==8
					|| matriz[i+1][j]==10){

						matriz[i+1][j]=0;	//destruye la mina
						matriz[i][j]=0;
						return 0;
					}
					else if (matriz[i+1][j]==0){
						//matriz[i+1][j]=4;	//desaparece la bala
						matriz[i][j]=0;
						return 0;
					}
				}
				else if(matriz[i][j]==4 && opcion==4){

					if(matriz[i-1][j]==9 || matriz[i-1][j]==3
					|| matriz[i-1][j]==6 || matriz[i-1][j]==7 || matriz[i-1][j]==8
					|| matriz[i-1][j]==10){

						matriz[i-1][j]=0;	//destruye la mina
						matriz[i][j]=0;
						return 0;
					}
					else if (matriz[i-1][j]==0){
						//matriz[i-1][j]=4;	//desaparece la bala
						matriz[i][j]=0;
						return 0;
				}

			}

		}
		}
	}

    return **matriz;
}


/*En esta funcion los agentes disparan o dejan una mina.
 la mina  o el disparo sera colocado en alguna de las 4 posiciones disponibles izquierda derecha arriba abajo
 la bala solo se podra poner donde la posicion sea cero si dispara y hay un jugador donde caera la bala esta lo
 atraviesa*/
int disparaAgente(int **matriz){
  int i,j;
  for(i=0; i<10; i++){
      for (j = 0; j < 10; j++){
          if (matriz[i][j]==3){///
              if (matriz[i-1][j]==2) {
                  matriz[i-2][j]=9;
                  return ;
              }else if (matriz[i][j-1]==2) {
                  matriz[i][j-2]=9;
                  return ;
              }else if (matriz[i][j+1]==2) {
                  matriz[i][j+2]=9;
                  return ;
              }else if (matriz[i][j-1]==2) {
                  matriz[i][j-2]=9;
                  return ;
              }

              else if(matriz[i][j-1]!=1 && matriz[i][j-1]!=2){
                  matriz[i][j-1]=9;
                  return ;
              }
              else if(matriz[i-1][j]!=1 && matriz[i-1][j]!=2){
                  matriz[i-1][j]=9;
              }
							else if(matriz[i+1][j]!=1 && matriz[i+1][j]!=2){
                  matriz[i+1][j]=9;
              }
							else if(matriz[i][j+1]!=1 && matriz[i][j+1]!=2){
                  matriz[i][j+1]=9;
                  return ;
              }
          }



          else if(matriz[i][j]==6){
									if (matriz[i-1][j]==2) {
											matriz[i-2][j]=9;
											return ;
									}else if (matriz[i][j-1]==2) {
											matriz[i][j-2]=9;
											return ;
									}else if (matriz[i][j+1]==2) {
											matriz[i][j+2]=9;
											return ;
									}else if (matriz[i][j-1]==2) {
											matriz[i][j-2]=9;
											return ;
									}



              else if(matriz[i-1][j]!=1 && matriz[i-1][j]!=2){
                  matriz[i-1][j]=9;

              }else if(matriz[i][j+1]!=1 && matriz[i][j+1]!=2){
                  matriz[i][j+1]=9;

              }else if(matriz[i][j-1]!=1 && matriz[i][j-1]!=2){
	                  matriz[i][j-1]=9;
	                  return ;
	            }else if(matriz[i+1][j]!=1 && matriz[i+1][j]!=2){
                  matriz[i+1][j]=9;
              }
          }



          else if(matriz[i][j]==8){//
								if (matriz[i-1][j]==2) {
										matriz[i-2][j]=9;
										return ;
								}else if (matriz[i][j-1]==2) {
										matriz[i][j-2]=9;
										return ;
								}else if (matriz[i][j+1]==2) {
										matriz[i][j+2]=9;
										return ;
								}else if (matriz[i][j-1]==2) {
										matriz[i][j-2]=9;
										return ;
								}

							 else if(matriz[i-1][j]!=1 && matriz[i-1][j]!=2){
									 matriz[i-1][j]=9;

							 }else if(matriz[i][j+1]!=1 && matriz[i][j+1]!=2){
									 matriz[i][j+1]=9;

							 }else if(matriz[i][j-1]!=1 && matriz[i][j-1]!=2){
										 matriz[i][j-1]=9;
										 return ;
							 }else if(matriz[i+1][j]!=1 && matriz[i+1][j]!=2){
									 matriz[i+1][j]=9;
							 }

          }



          else if(matriz[i][j]==7){
									if (matriz[i-1][j]==2) {
											matriz[i-2][j]=9;
											return ;
									}else if (matriz[i][j-1]==2) {
											matriz[i][j-2]=9;
											return ;
									}else if (matriz[i][j+1]==2) {
											matriz[i][j+2]=9;
											return ;
									}else if (matriz[i][j-1]==2) {
											matriz[i][j-2]=9;
											return ;
									}

								 else if(matriz[i-1][j]!=1 && matriz[i-1][j]!=2){
										 matriz[i-1][j]=9;

								 }else if(matriz[i][j+1]!=1 && matriz[i][j+1]!=2){
										 matriz[i][j+1]=9;

								 }else if(matriz[i][j-1]!=1 && matriz[i][j-1]!=2){
											 matriz[i][j-1]=9;
											 return ;
								 }else if(matriz[i+1][j]!=1 && matriz[i+1][j]!=2){
										 matriz[i+1][j]=9;
								 }




					}else if(matriz[i][j]==10){
										if (matriz[i-1][j]==2) {
												matriz[i-2][j]=9;
												return ;
										}else if (matriz[i][j-1]==2) {
												matriz[i][j-2]=9;
												return ;
										}else if (matriz[i][j+1]==2) {
												matriz[i][j+2]=9;
												return ;
										}else if (matriz[i][j-1]==2) {
												matriz[i][j-2]=9;
												return ;
										}

									 else if(matriz[i-1][j]!=1 && matriz[i-1][j]!=2){
											 matriz[i-1][j]=9;

									 }else if(matriz[i][j+1]!=1 && matriz[i][j+1]!=2){
											 matriz[i][j+1]=9;

									 }else if(matriz[i][j-1]!=1 && matriz[i][j-1]!=2){
												 matriz[i][j-1]=9;
												 return ;
									 }else if(matriz[i+1][j]!=1 && matriz[i+1][j]!=2){
											 matriz[i+1][j]=9;
									 }
        }
      }
  }
  return **matriz;
}

/*Si hay un agente en el campo de juego este retornara true si no
hay retorna false*/
int EncontrarAgente(int **matriz,int Agente){
  int i,j;
  for (i=0;i<10;i++){
      for (j = 0; j < 10; j++) {
          if(matriz[i][j]==Agente){
              return TRUE;
          }
      }
  }
  return FALSE;
}

/*esta funcion me dira si hay una bala de una gente en el terreno de juego
true si la hay false si no hay si al recorrer la matriz no encontramos ningun 9 que es el numero que representa
las balas enemigas*/
int hayBalas(int **matriz){//si hay balas de algun agente
    int i,j;
    for (i=0;i<10;i++){
        for (j = 0; j < 10; j++) {
            if(matriz[i][j]==9){
                return TRUE;
            }
        }
    }
    return FALSE;
}


/*Esta funcion destruye una bala agente es usada en el main para que una vez que el agente que disparo esa
bala muere o se acaban sus 5 segundos su mina tambien muere*/
int destruirbala(int **matriz){
  int i,j;
  for (i=0;i<10;i++){
      for (j = 0; j < 10; j++) {
        if(matriz[i][j]==9){
            matriz[i][j]=0;
        }
      }
  }
}


/*Esta funcion la uso en el main lo que hace es que destruye el botin de balas
en el main es utilizada para que si el jugador no tomo el botin ya han pasado 5 segundos el botin desaparesca
de donde estaba y reaparesca en otro lugar*/
int destruirbotin(int **matriz){
  int i,j;
  for (i=0;i<10;i++){
      for (j = 0; j < 10; j++) {
        if(matriz[i][j]==5){
            matriz[i][j]=0;
        }
      }
  }
}


/*Como cree una matriz de enteros dependiendo del entero que encuentre
al ir recorriendo la matriz imprimo un caracter string*/
void ImprimeMatriz(int **matriz){
    int i,j;
    for (i=0;i<10;i++){
        for (j = 0; j < 10; j++) {
             if (matriz[i][j] == 1) {
                printf("X ");
            } else if (matriz[i][j] == 3) {
                printf("T ");
            } else if (matriz[i][j] == 2) {
                printf("@ ");
            }else if (matriz[i][j] == 4) {
                printf("* ");
            }else if (matriz[i][j] == 5){
                printf("B ");
            }else if (matriz[i][j] == 6) {
                printf("& ");
            }
            else if (matriz[i][j] == 7) {
                printf("$ ");
            }else if (matriz[i][j] == 8) {
                printf("# ");
            }else if (matriz[i][j] == 9) {
                printf("+ ");
            }else if (matriz[i][j] == 10) {
                printf("k ");
            }else {
                printf("O ");
            }
        }
        printf("\n");
    }
}


/*Esta funcion limpia la memoria que se utilizo para crear la matriz dinamica con malloc
lipia filas y columnas*/
void LimpiarMatriz(int **matriz){
    int i,j;
    for(i=0;i<10;i++){
            free(matriz[i]);
    }
    free(matriz);
}


/*Con esta funcion no damos cuenta cuando un agente murio true hay un agente vivo en el terreno de juego
false si no hay ninguno vivo en el terreno*/
//para saber cuando un agente murio
int MurioAgente(int **matriz){
    int i,j;
    for (i=0;i<10;i++){
        for (j = 0; j < 10; j++) {
            if(matriz[i][j]==3 || matriz[i][j]==6 || matriz[i][j]==7 || matriz[i][j]==8 || matriz[i][j]==10){
                return TRUE;
            }

        }
    }
    return FALSE;
}

/*Esta funcion retorna true si en el terreno de jugo hay un botin de balas y
false si no*/
int tomabalas(int **matriz){
    int i,j;
    for (i=0;i<10;i++){
        for (j = 0; j < 10; j++) {
            if(matriz[i][j]==5){
                return TRUE;
            }
        }
    }
    return FALSE;
}


/*esta funcion pondra un botin de balas aleatorio en el terreno de juego*/
int ramdomBalas(int **matriz){
    int f =10,c=10;
    int X,Y,i,j;
    X= rand() % (f-1);
    Y= rand() % (c-1);
    for (i=0;i<10;i++){
        for (j = 0; j < 10; j++) {
            if (matriz[X][Y]==0){
                matriz[X][Y]=5;
            }
        }
    }
    return **matriz;
}



/*estas funciones son usadas para crear una lista doblemente enlazada la cual contendra el camino correcto del laberinto*/
//Función para crear un nuevo nodo
NODO *CrearNodo(int dato){
    NODO* nuevo = NULL;

    nuevo = (NODO*)malloc(sizeof(NODO));
    if( nuevo != NULL)
    {
        nuevo->dato = dato;
        nuevo->siguiente = NULL;
        nuevo->anterior = NULL;
    }
    return nuevo;
}

/*int BuscarEnLista(NODO **cabeza, int dato){
    NODO *nuevo = NULL;
    for(nuevo= cabeza; nuevo != NULL; nuevo->siguiente){
        if(nuevo->dato == dato){
            return nuevo;
        }
    }
    return NULL;
}*/


//Función para insertar al inicio de la lista
int InsertarInicio(NODO **cabeza, int dato)
{
    NODO *nuevo = NULL;

    nuevo = CrearNodo(dato);
    if (nuevo != NULL){
        nuevo->siguiente = *cabeza;
        nuevo->anterior = NULL;
        if( *cabeza != NULL)
            (*cabeza)->anterior = nuevo;
        *cabeza = nuevo;
        return 1;
    }
    return 0;
}

//Función para insertar al final de la lista
int InsertarFinal(NODO **cabeza, int dato){
    NODO *nuevo = NULL, *nAux = *cabeza;

    nuevo = CrearNodo(dato);
    if (nuevo != NULL){
        while(nAux->siguiente != NULL){
            nAux = nAux->siguiente;
        }
        nuevo->anterior = nAux;
        nAux->siguiente = nuevo;
        return 1;
    }
    return 0;
}

//Función para imprimir la lista
void ImprimirLista(NODO *cabeza){
    NODO *nAux = cabeza;

    while(nAux != NULL){
        printf("%d ", nAux->dato);
        nAux = nAux->siguiente;
    }
}

/*funcion en la cual metemos la solucion del laberinto en una lista doblemente
enlazada y la imprimimos*/
void SolucionLaberinto(int **matriz){
    NODO *cabeza = NULL;

    InsertarInicio(&cabeza, matriz[1][0]);
    InsertarFinal(&cabeza, matriz[1][1]);
    InsertarFinal(&cabeza, matriz[1][2]);
    InsertarFinal(&cabeza, matriz[1][3]);
    InsertarFinal(&cabeza, matriz[2][3]);
    InsertarFinal(&cabeza, matriz[4][3]);
    InsertarFinal(&cabeza, matriz[5][3]);
    InsertarFinal(&cabeza, matriz[6][3]);
    InsertarFinal(&cabeza, matriz[6][2]);
    InsertarFinal(&cabeza, matriz[7][2]);
    InsertarFinal(&cabeza, matriz[8][2]);
    InsertarFinal(&cabeza, matriz[8][3]);
    InsertarFinal(&cabeza, matriz[8][4]);
    InsertarFinal(&cabeza, matriz[8][5]);
    InsertarFinal(&cabeza, matriz[8][6]);
    InsertarFinal(&cabeza, matriz[8][7]);
    InsertarFinal(&cabeza, matriz[7][7]);
    InsertarFinal(&cabeza, matriz[6][7]);
    InsertarFinal(&cabeza, matriz[5][7]);
    InsertarFinal(&cabeza, matriz[4][7]);
    InsertarFinal(&cabeza, matriz[3][7]);
    InsertarFinal(&cabeza, matriz[3][8]);
    InsertarFinal(&cabeza, matriz[3][9]);
    //BuscarEnLista(&cabeza, matriz[8][3]);

    ImprimirLista(cabeza);
    printf("\n");
}

/*funcion en la guardamos la solucion de el laberintto del primer nivel y los immprimos cuando el jugdor
desee verlo*/
void MostrarSolucion(){
    int i,j;
    int matriz[10][10] = {1,1,1,1,1,1,1,1,1,1,
                        0,0,0,0,1,1,1,1,1,1,
                        1,1,1,0,1,1,1,1,1,1,
                        1,1,1,0,1,1,1,0,0,0,
                        1,1,1,0,1,1,1,0,1,1,
                        1,1,1,0,1,1,1,0,1,1,
                        1,1,0,0,1,1,1,0,1,1,
                        1,1,0,1,1,1,1,0,1,1,
                        1,1,0,0,0,0,0,0,1,1,
                        1,1,1,1,1,1,1,1,1,1};
   for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            if (matriz[i][j] == 1) {
                printf("x ");
            } else {
                printf("O ");
            }
        }
        printf("\n");
    }
    printf("\n");
}

//se meten los agentes en la pila
void Agentes_Muertos(char Agente){
  Pila pila = NULL;
	Push(&pila, Agente);
  //Push(&pila, Agente);
  //'x'
	//printf("%c, ", Pop(&pila));
	//printf("%c, ", Pop(&pila));
	printf("%c\n ", Pop(&pila));
}
//sacamos lo agentes de la pila
void MostrarAgentesMuertos(char Agente){
  Pila pila = NULL;
  printf("%c, ", Pop(&pila));
}


/*funcion para meter elementos en la pila en este caso lo agentes*/
void Push(Pila *pila, char v){
	pNodo nuevo;
	/* Crear un nodo nuevo */
	nuevo = (pNodo)malloc(sizeof(tipoNodo));
	nuevo->valor = v;
	/* Añadimos la pila a continuación del nuevo nodo */
	nuevo->siguiente = *pila;
	/* Ahora, el comienzo de nuestra pila es en nuevo nodo */
	*pila = nuevo;
}


/*funcion con la cual sacaremos los elementos que hayamos metido en la pila en este caso los agentes*/
char Pop(Pila *pila){
	pNodo nodo;
	/* variable auxiliar para manipular nodo */
	char v;
	 /* variable auxiliar para retorno */
	 /* Nodo apunta al primer elemento de la pila */
	 nodo = *pila;
	 if(!nodo) return 0;
	 /* Si no hay nodos en la pila retornamos 0*/
	 /* Asignamos a pila toda la pila menos el primer elemento */
	 *pila = nodo->siguiente;
	 /* Guardamos el valor de retorno */
	 v = nodo->valor;
	 /* Borrar el nodo */
	 free(nodo);
	 return v;
}


/*CURL DESCARGAR ARCHIVOS PDF estos archivos se descargaran en la carpeta del proyecto*/
void DescargarPDF(){
	size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
	}

		CURL *curl;
		CURLcode res;
		FILE *fp;
		FILE *tp;

		curl = curl_easy_init();
		fp = fopen("2015-2016-PLENARIO-SESION-9.pdf","wb");//abrimos arhivo .pdf donde se guardara el pdf descargado

		//link de descarga del pdf
		curl_easy_setopt(curl, CURLOPT_URL,"http://www.asamblea.go.cr/Actas/2015-2016-PLENARIO-SESI%C3%93N-9.pdf");
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); // la informacion del pdf en el archivo fp
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);//la escrimos aca
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);

		fclose(fp);//cerramos el archivo

}

/*esta funcion contara cuantas palabras tiene un archivo txt nos ayudara para recorrer todo l archivobuscando los vervos
sustantivos y articulos esta funcion se usa en compara*/
int cuantas_palabras_hay(){
  FILE *pfsArchivo;
  int nContador=0;
  char espacio[32]="";
  pfsArchivo= fopen("texto.txt", "r");
  if (pfsArchivo== NULL)
  printf("No se pudo abrir el archivo %s \n", "prueba.txt");
  else{
    while (fscanf(pfsArchivo,"%32s", espacio) != EOF){
      nContador++;
    };
    return nContador;
    fclose(pfsArchivo);
  }
}

/*esta funcion busca todos los vervos que hayan en un archivo.txt
para ello usa la terminacion ar er ir ando endo iendo*/
void Compara(){
  printf("\nLECTOR\n");
  int CantidaDePalabras = cuantas_palabras_hay();
  //int i = 0;
  int cuentaP=0;
  int contador=0;
	int cuentaVerbos=0;
	FILE* miarchivo = NULL;
	char* nombrearchivo = "texto.txt";
	char lectura[10];
	//char cadena_c[25] = "el cagar rico";

	miarchivo = fopen(nombrearchivo, "r");	//leer
	if (miarchivo == NULL){
      printf("No se pudo abrir el archivo %s \n", "texto.txt");
  }
 printf("Los vervos encontrados son: \n");
  while (contador<CantidaDePalabras) {
    int i = 0 ;
    fscanf(miarchivo, "%32s",lectura);
    while (lectura[i] != '\0'){
      if (lectura[i] == 'e' && lectura[i+1] == 'r' && lectura[i+2] == '\0'){
					//printf("%s\n",lectura);
					cuentaVerbos++;
      }else if (lectura[i] == 'a' && lectura[i+1] == 'r' && lectura[i+2] == '\0') {
          //printf("%s\n",lectura);
					cuentaVerbos++;
      }else if (lectura[i] == 'i' && lectura[i+1] == 'r' && lectura[i+2] == '\0') {
          //("%s\n",lectura);
					cuentaVerbos++;
      }else if (lectura[i] == 'a' && lectura[i+1] == 'n' && lectura[i+2] == 'd' && lectura[i+3] == 'o' && lectura[i+4] == '\0') {
          //printf("%s\n",lectura);
					cuentaVerbos++;
      }else if (lectura[i] == 'e' && lectura[i+1] == 'n' && lectura[i+2] == 'd' && lectura[i+3] == 'o' && lectura[i+4] == '\0') {
          //printf("%s\n",lectura);
					cuentaVerbos++;
      }else if (lectura[i] == 'i' && lectura[i+1] == 'e' && lectura[i+2] == 'n' && lectura[i+3] == 'd'  && lectura[i+3] == 'o' && lectura[i+4] == '\0') {
          //printf("%s\n",lectura);
					cuentaVerbos++;
      }
      i++;
    }
    /*if (strcmp(lectura,"gato")==0) {
          cuentaP++;
    }
  	//printf("%s\n",lectura);*/
    contador++;
  }
	printf("Este es el numero de verbos que se leyeron %d\n",	cuentaVerbos );
  //printf("Esta palabra salio %i veces\n",cuentaP);
	fclose(miarchivo);
}



/*esta funcion busca todos los vervos que hayan en un archivo.txt
para ello usa la terminacion ar er ir ando endo iendo*/
/*
void Compara(){
  printf("\nLECTOR\n");
  int CantidaDePalabras = cuantas_palabras_hay();
  //int i = 0;
  int cuentaP=0;
  int contador=0;
	int cuentaVerbos=0;
	FILE* miarchivo = NULL;
	char* nombrearchivo = "texto.txt";
	char lectura[10];
	int listaVebos={"indagar","encaminando","Poder","partir","hacer","reconocer",
	"amparar","Siendo","examinar","contar"};
  //char cadena_c[25] = "el cagar rico";

	miarchivo = fopen(nombrearchivo, "r");	//leer
	if (miarchivo == NULL){
      printf("No se pudo abrir el archivo %s \n", "texto.txt");
  }
 printf("Los vervos encontrados son: \n");
  while (contador<CantidaDePalabras) {
    int i = 0 ;cuenta=0;
    fscanf(miarchivo, "%32s",lectura);
    while (lectura[i] != '\0'){
      if (lectura[i] == 'e' && lectura[i+1] == 'r' && lectura[i+2] == '\0'){
					printf("%s\n",lectura);
					if (lectura == listaVebos[cuenta]){
						cuentaVerbos++;
						cuenta++;
					}

      }else if (lectura[i] == 'a' && lectura[i+1] == 'r' && lectura[i+2] == '\0') {
          printf("%s\n",lectura);
					if (lectura == listaVebos[cuenta]){
						cuentaVerbos++;
						cuenta++;
					}
      }else if (lectura[i] == 'i' && lectura[i+1] == 'r' && lectura[i+2] == '\0') {
          printf("%s\n",lectura);
					if (lectura == listaVebos[cuenta]){
						cuentaVerbos++;
						cuenta++;
					}
      }else if (lectura[i] == 'a' && lectura[i+1] == 'n' && lectura[i+2] == 'd' && lectura[i+3] == 'o' && lectura[i+4] == '\0') {
          printf("%s\n",lectura);
						cuentaVerbos++;
      }else if (lectura[i] == 'e' && lectura[i+1] == 'n' && lectura[i+2] == 'd' && lectura[i+3] == 'o' && lectura[i+4] == '\0') {
          printf("%s\n",lectura);
					if (lectura == listaVebos[cuenta]){
						cuentaVerbos++;
						cuenta++;
					}
      }else if (lectura[i] == 'i' && lectura[i+1] == 'e' && lectura[i+2] == 'n' && lectura[i+3] == 'd'  && lectura[i+3] == 'o' && lectura[i+4] == '\0') {
          printf("%s\n",lectura);
					if (lectura == listaVebos[cuenta]){
						cuentaVerbos++;
						cuenta++;
					}
      }
      i++;
    }
    //if (strcmp(lectura,"gato")==0) {
  //        cuentaP++;
    //}
  	//printf("%s\n",lectura);
    //contador++;
  //}
	printf("%d\n",	cuentaVerbos );
  //printf("Esta palabra salio %i veces\n",cuentaP);
	fclose(miarchivo);
}
*/